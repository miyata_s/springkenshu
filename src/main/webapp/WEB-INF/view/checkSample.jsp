<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<p>${message}</p>

         <%--「${pageContext.request.contextPath}」はEL式の暗黙オブジェクトです。 --%>
	<form:form modelAttribute="checkForm" action="${pageContext.request.contextPath}/sample/check/info/">

   		   <%--ceckboxesはinput type="chexckbox"を複数作成してくれる --%>
   		   <%--複数と言うのは、「items="${checkEmployees}"」に指定した要素数に準拠する --%>
   		   <%--delimiter,これは単純に複数要素を表示する際の表示分けに、どんな文字列を使用するかを定義。省略すれば、何も表示されない。 --%>
   		 <form:checkboxes path="employees" items="${checkEmployees}" delimiter="/" />
  		 <input type="submit" />
	</form:form>
</body>
</html>